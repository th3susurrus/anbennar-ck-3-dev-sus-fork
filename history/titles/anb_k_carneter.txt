k_carneter = {
	
	590.9.26 = {
		holder = rubentis_0001 # Ruben I Rubentis
	}
	595.2.18 = {
		holder = rubentis_0004 # Ruben II Rubentis
	}
	600.3.16 = {
		holder = rubentis_0005 # Emmeran I Rubentis
	}
	635.3.12 = {
		holder = rubentis_0006 # Ruben III Rubentis
	}
	667.6.22 = {
		holder = rubentis_0007 # Emmeran II Rubentis
	}
	698.1.28 = {
		holder = rubentis_0008 # Lorenan I Rubentis
	}
	705.9.14 = {
		holder = rubentis_0011 # Ruben IV Rubentis
	}
	720.8.2 = {
		holder = rubentis_0012 # Ruben V "the Wise" Rubentis
	}
	770.7.31 = {
		holder = rubentis_0013 # Lorenan II Rubentis
	}
	783.10.15 = {
		holder = rubentis_0018 # Ruben VI Rubentis
	}
	801.3.18 = {
		holder = rubentis_0019 # Emmeran III Rubentis
	}
	831.9.12 = {
		holder = rubentis_0020 # Ruben VII Rubentis
	}
	861.12.7 = {
		holder = rubentis_0022 # Lorenan III Rubentis
	}
	897.8.30 = {
		holder = rubentis_0023 # Emmeran IV Rubentis
	}
	899.10.30 = { # Dameris Conquest of Carneter
		holder = dameris_0005 # Canrec "Moonborn" Dameris
	}
	915.10.30 = {
		holder = dameris_0006 # Aucanus Dameris
	}
	940.2.2 = {
		holder = dameris_0007 # Arrel Dameris
	}
	955.10.2 = {
		holder = dameris_0011 # Galien Dameris
	}
	980.12.1 = {
		holder = dameris_0013 # Marven Dameris
	}
	998.2.1 = {
		holder = dameris_0012 # Auci Dameris
	}
	1021.10.3 = {
		holder = dameris_0014 # Crege Dameris
	}
}

d_carneter = {
	1000.1.1 = { change_development_level = 8 }
	1021.10.3 = {
		holder = dameris_0014 # Crege Dameris
	}
}

c_carneter = {
	1021.10.3 = {
		holder = dameris_0014 # Crege Dameris
	}
}

c_woodwell = {
	1001.1.1 = { change_development_level = 7 }
	1021.10.3 = {
		holder = dameris_0014 # Crege Dameris
	}
}

c_timberfort = {
	1001.1.1 = { change_development_level = 7 }
}

c_ancards_crossing = {
	1021.10.3 = {
		holder = dameris_0014 # Crege Dameris
	}
}

c_ilvandet = {
	474.1.1 = {
		liege = k_carneter
	}		
	972.3.28 = {
		holder = deruwris_0001
	}
	994.1.14 = {
		holder = deruwris_0002
	}
	1013.10.3 = {
		holder = ilvan_0001 # Lucius síl Ilvan
	}
}

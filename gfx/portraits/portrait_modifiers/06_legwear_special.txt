﻿legwear_special = {

	usage = game
	selection_behavior = max
	priority = 10


	no_legwear_race = {
		usage = game 
		dna_modifiers = {
			accessory = {
				mode = add
				gene = legwear
				template = no_legwear
				value = 0
			}
		}
		weight = {
			base = 0
			modifier = {
				add = 2000
				culture = culture:redscale
			}
		}
	}

}